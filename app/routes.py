from app import app

@app.route('/')
@app.route('/index')
def index():
    return "Hello, World!"


@app.route('/info/<state>')
def info(state):
    return "Info for " + state